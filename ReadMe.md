# QuickADUserInfo

A graphical Powershell script (using WPF/XAML) that performs Active Directory queries across multiple domains,
to display basic information like "is user account locked?", "is password expired?", etc.

You can easily adjust the script to your environment by replacing the placeholder domain names in the array `$domains`.

Requires the _Remote Server Administration Tools (RSAT)_; see comments in the script for hints on how to install it.

![Main window](screenshots/v1-a.png)

![User selection](screenshots/v1-b.png)

![Result window](screenshots/v1-c.png)

Copyright © 2020 Sascha Offe (so@saoe.net).  
Published under the terms of the [MIT license](http://opensource.org/licenses/MIT>) (see accompanying file [License.txt](License.txt) for details).
