<#
┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                          QuickADUserInfo                                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────┘
Copyright © 2020 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license. See accompanied file "License.txt" for details.

A Powershell script with a GUI, that can perform an Active Directory query across multiple domains
to present basic information like "is user account locked?", "is password expired?", etc.

Adjust to your environment by replacing the placeholder domain names in $domains.

Remarks:

 I. This little helper is just to show the data and has no means to change those states itself.

II. Requires the RSAT to be installed on the machine where the script is executed.

	If you are using it on a Windows server then you already have RSAT available.
	If you are using it on a Windows workstation then you need to install the 'Remote Server Administration Tools' (RSAT) package.

	a) RSAT for Windows 10 1803 or older (including Windows 7 and Windows 8.1)
		Download and install: https://www.microsoft.com/en-us/download/details.aspx?id=45520

		Enable the module via GUI after installation:
			Control Panel > Programs > Turn Windows Features On or Off > Remote Server Administration Tools > ...
			... Role Administration Tools > AD DS and AD LDS Tools > Active Directory module for Windows PowerShell.

	b) RSAT for Windows 10 1809 or newer
		Available as an optional feature for Windows; there's no need to download an external package.

		Install/Enable via GUI:
			Settings > Apps > Manage Optional Features > Add features > RSAT: Active Directory Domain Services and Lightweight Directory Tools > Install.

		Install/Enable via PowerShell:
			Add-WindowsCapability -online -Name "Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0"

	Note: You can install RSAT only in the Professional, Education, and Enterprise Windows editions,
		  but not on Windows Home or Single Language.

History:
2020-04-23 | Sascha Offe: Created.
2020-04-30 | Sascha Offe: Switch from WinForms to WPF/XAML for the GUI, plus some code refactoring.
2020-05-04 | Sascha Offe: A few tweaks and special-case/error/exception handling -- now ready for a first release, I think...
#>

#Requires -Version 3
    # IMPORTANT: The line is _not_ a comment, just weird Powershell syntax; leave it as it is!

$script_and_version           = "QuickADUserInfo 1.0"
$TextBox_Username_InitialText = "(Wildcards are allowed; e.g. *name*)"

$domains = @(
    "aaa.example.net",
    "bbb.example.net",
    "ccc.example.net",
    "ddd.example.net",
    "eee.example.net",
    "fff.example.net",
    "ggg.example.net",
    "hhh.example.net"
)

# --------------------------------------------------------------------------------------------------
# Loading .NET assemblies, importing modules, preparing misc. variables...

Add-Type -AssemblyName PresentationFramework
Add-Type -AssemblyName PresentationCore
Add-Type -AssemblyName WindowsBase
Add-Type -AssemblyName System.Drawing

$is_any_checkbox_checked = $false
$font                    = New-Object System.Drawing.Font("Calibri", 14)
$mono_font               = New-Object System.Drawing.Font("Consolas", $font.Size)

Try
{
	Import-Module ActiveDirectory -ErrorAction Stop
}
Catch
{
	$null = [System.Windows.MessageBox]::Show("The ActiveDirectory module failed to load.`n`nInstall the 'Remote Server Administration Tools' (RSAT) and try again.", "Import-Module Error", "OK", "Error")
	Write-Error "The ActiveDirectory module failed to load. Install the RSAT module and try again."
	Exit
}

# --------------------------------------------------------------------------------------------------
# The XAML string for the GUI layout and style.

[xml] $MainWindowXaml = @"
<Window
	xmlns                 = "http://schemas.microsoft.com/winfx/2006/xaml/presentation"
	xmlns:x               = "http://schemas.microsoft.com/winfx/2006/xaml"
	Title                 = "$script_and_version"
	x:Name                = "MainWindow"
	WindowStartupLocation = "CenterScreen"
	SizeToContent         = "WidthAndHeight"
	ResizeMode            = "CanMinimize"
>
	<Window.Background>
		<LinearGradientBrush StartPoint="0, 0" EndPoint="1, 1">
			<GradientStop Color="#F9F8FE" Offset="0" />
			<GradientStop Color="#E6E6FA" Offset="0.5" />
		</LinearGradientBrush>
	</Window.Background>

	<StackPanel
		Orientation = "Horizontal"
		x:Name      = "StackPanel1"
	>
		
		<!-- **** Left column: Checkboxes ****************************************************** -->
		<StackPanel
			Orientation = "Vertical"
			Margin      = "10, 10, 10, 10"
		>
			<CheckBox
				Name    = "CheckBox_All"
				Content = "All domains"
				Margin  = "0, 2.5"
			/>

			<!-- This panel will contain the later auto-generated checkboxes -->
			<StackPanel
				x:Name      = "CheckBoxPanel"
				Orientation = "Vertical"
			/>
		</StackPanel>
		
		<!-- **** Middle column: Username input ************************************************ -->
		<StackPanel
			Orientation = "Vertical"
			Margin      = "10, 10, 10, 10"
		>
			<Label
				x:Name                     = "Label_Username"
				HorizontalContentAlignment = "Left"
				DockPanel.Dock             = "Top"
				Content                    = "Username:"
			/>

			<TextBox
				x:Name                     = "TextBox_Username"
				Text                       = "$TextBox_Username_InitialText"
				Foreground                 = "Gray"
				Padding                    = "4"
				Width                      = "400"
				HorizontalContentAlignment = "Left"
				DockPanel.Dock             = "Top"
			/>
		</StackPanel>

		<!-- **** Right column: OK/Cancel buttons ********************************************** -->
		<StackPanel
			Orientation = "Vertical"
			Margin      = "10, 10, 10, 10"
		>
			<Button
				x:Name         = "Button_OK"
				Content        = "OK"
				IsEnabled      = "False"
				DockPanel.Dock = "Top"
				Margin         = "0, 0, 0, 5"
				Padding        = "50, 10, 50, 10"
			/>

			<Button
				x:Name         = "Button_Cancel"
				Content        = "Cancel"
				IsCancel       = "True"
				DockPanel.Dock = "Top"
				Margin         = "0, 5, 0, 0"
				Padding        = "50, 10, 50, 10"
			/>
		</StackPanel>
	</StackPanel>
</Window>
"@


[xml] $InfoWindowXaml = @"
<Window
	xmlns                 = "http://schemas.microsoft.com/winfx/2006/xaml/presentation"
	xmlns:x               = "http://schemas.microsoft.com/winfx/2006/xaml"
	x:Name                = "InfoWindow"
	WindowStartupLocation = "CenterScreen"
	SizeToContent         = "WidthAndHeight"
	ResizeMode            = "CanMinimize"
>

<Window.Background>
	<LinearGradientBrush>
		<GradientStop Color="#E6E6FA" Offset="0" />
		<GradientStop Color="#F9F8FE" Offset="1" />
	</LinearGradientBrush>
</Window.Background>

	<StackPanel
		Orientation = "Vertical"
		x:Name      = "StackPanel1"
	>
		
		<!-- This will later be auto-filled with data. -->
		<Grid
			x:Name              = "InfoWindow_Grid"
		/>

		<Button
			x:Name         = "InfoWindow_Button_OK"
			Content        = "OK"
			IsDefault      = "True"
			DockPanel.Dock = "Bottom"
			Margin         = "10, 10, 10, 10"
			Padding        = "10"
		/>
	</StackPanel>
</Window>
"@


# --------------------------------------------------------------------------------------------------
# Prepare the main window by processing the XAML and finding all controls in it and store them in PowerShell variables.

$MainWindowReader = (New-Object System.Xml.XmlNodeReader $MainWindowXaml)
$MainWin = [Windows.Markup.XamlReader]::Load($MainWindowReader)

$MainWindowXaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
	Set-Variable -Name $_.Name -Value $MainWin.FindName($_.Name) -Force -Scope Script
}


# --------------------------------------------------------------------------------------------------
# This function automatically generates the checkboxes on the left, based on the entries in the $domains array.
# Returns an array of those checkboxes, for further processing.

function generateCheckboxes
{
    $CheckBoxCounter = 1
 
    $CheckBoxes = foreach ($entry in $script:domains)
    {
		$NewCheckBox = New-Object System.Windows.Controls.CheckBox
		$NewCheckBox.Name = "CheckBox$CheckBoxCounter" # Give it a unique name.
		$NewCheckBox.Content = $entry
		$NewCheckBox.Margin  = "20, 2.5"
		
        $NewCheckBox.add_Checked( { checkState } )
		$NewCheckBox.add_UnChecked( { checkState } )

	    $CheckBoxPanel.AddChild($NewCheckBox)
        $CheckBoxCounter++	    
	    
        $NewCheckBox # return object ref to array
    }

    $CheckBoxes
}


# --------------------------------------------------------------------------------------------------
# Setup the left column of checkboxes and save the returned array of checkboxes for later.

$checkboxes_domains = generateCheckboxes


# --------------------------------------------------------------------------------------------------
# Only if at least one domain checkbox is selected and the username textbox is not empty,
# then is makes sense to enable the OK button to start a search.
# 
# This function used by the event handlers of the checkboxes and textbox.

function checkState
{
	ForEach ($c in $checkboxes_domains)
	{
		if ($c.IsChecked)
		{
			$script:is_any_checkbox_checked = $true
			break
		}
		else
		{
			$script:is_any_checkbox_checked = $false
		}
	}

    if ($is_any_checkbox_checked -and (-not [string]::IsNullOrEmpty($TextBox_Username.Text)) -and (-not ($TextBox_Username.Text -eq $TextBox_Username_InitialText)))
    {
       $Button_OK.IsEnabled = $true
    }
    else
    {
        $Button_OK.IsEnabled = $false
    }
}


# --------------------------------------------------------------------------------------------------
# Get user data from the AD of the selected domains.
#
# If only one match for the searched username is found, that one is returned directly.
# For multiple matches for the searched username, a Out-GridView is presented, and the user has to select one entry from it.

function getUserInfo
{
    Param(
        [parameter(Mandatory=$true)] [String] $user,
        [parameter(Mandatory=$true)] [String[]] $domains
    )
	
    $properties = @(
		"GivenName",
		"Surname",
		"sAMAccountname",
		"userPrincipalName",
		"Description",
		"Enabled",
		"LockedOut",
		"CannotChangePassword",
		"PasswordExpired",
		"PasswordNeverExpires",
		"PasswordLastSet",
		"msDS-UserPasswordExpiryTimeComputed"
    )

    $users = @()
	
	foreach ($d in $domains)
    {
        if ($user.Contains('*'))
        {
            Try
			{
				$u = Get-ADUser -Filter "(sAMAccountname -like '$user') -or (GivenName -like '$user') -or (Surname -like '$user')" -Server $d -Properties $properties | select $properties
				$users += $u
			}
			Catch
			{
				$null = [System.Windows.MessageBox]::Show("Error querying domain $d.`n`nDomain: $d", "Error $d", "OK", "Error")
				continue
			}
        }
        else
        {
			Try
			{
				$u = Get-ADUser -Filter "(sAMAccountname -eq '$user') -or (GivenName -eq '$user') -or (Surname -eq '$user')" -Server $d -Properties $properties | select $properties
				$users += $u
			}
			Catch
			{
				$null = [System.Windows.MessageBox]::Show("Error querying domain $d", "Error $d", "OK", "Error")
				continue
			}
        }       
    }
	
    if ($users.Count -eq 1)
    {
		$selection = $users
    }
    elseif ($users.Count -gt 1)
    {
		$selection = $users | Out-GridView -Title "Please select an item" -OutputMode Single
    }
    else
    {
		$selection = $null
    }
   
	$selection
}


# --------------------------------------------------------------------------------------------------
# Function to display the search result (data of a single account)

function InfoWindow
{
	Param(
		[parameter(Mandatory=$true)] [AllowNull()] [Microsoft.ActiveDirectory.Management.ADUser] $user
    )
	
	# Parse the XAML and create variables for each found control.
	$InfoWindowReader = (New-Object System.Xml.XmlNodeReader $InfoWindowXaml)
	$InfoWin = [Windows.Markup.XamlReader]::Load($InfoWindowReader)

	$InfoWindowXaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
		Set-Variable -Name $_.Name -Value $InfoWin.FindName($_.Name) -Force -Scope Script
	}
	
	if (-not ($user -eq $null))
	{
		$InfoWin.Title      = $user.UserPrincipalName
		$InfoWin.FontFamily = $font.Name
		$InfoWin.FontSize   = $font.Size
		
		$PasswordExpiryDate = (New-Object System.DateTime $user."msDS-UserPasswordExpiryTimeComputed")
		$PasswordExpiryDate = $PasswordExpiryDate.AddYears(1600)
			# After converting the data from FileTime to DateTime, we have to add 1600 to get the correct year:
			# In the Active Directory, Microsoft specifies attributs that deal with time as a Windows FileTime.
			#
			# "The FILETIME structure records time in the form of 100-nanosecond intervals since January 1, 1601."
			# -- https://devblogs.microsoft.com/oldnewthing/20090306-00/?p=18913
		
		$UserInfo = @(
		    , @( "User Principal Name"       , $user.UserPrincipalName )
			, @( "Name"                      , ($user.GivenName + " " + $user.Surname) )
			, @( "Description"               , $($user.Description) )
			, @( "Account enabled"           , $user.Enabled )
			, @( "Account unlocked"          , -not ($user.LockedOut) )
			, @( "Password not expired"      , -not ($user.PasswordExpired) )
			, @( "Password can expire"       , -not ($user.PasswordNeverExpires) )
			, @( "Password expires on"       , ($PasswordExpiryDate | Get-Date -UFormat "%Y-%m-%d %H:%M:%S (%Z)") )
			, @( "Password last set"         , ($user.PasswordLastSet | Get-Date -UFormat "%Y-%m-%d %H:%M:%S (%Z)") )
			, @( "User can change password"  , -not ($user.CannotChangePassword) )
		)
		
		# --------------------------------------------------------------------------------------
		# Generate grid content...
		
		$i = 0
		
		foreach ($ui in $UserInfo)
		{
			# Seems like it must be in that order. It all looks a bit fishy, but it works...
			
			# 1. Creating new objects
			$NewRow                = New-Object System.Windows.Controls.RowDefinition
			$NewColumn_Description = New-Object System.Windows.Controls.ColumnDefinition
			$NewColumn_Content     = New-Object System.Windows.Controls.ColumnDefinition
			
			$NewColumn_Description.Width = "Auto"
			$NewColumn_Content.Width = "Auto"
			
			# 2. Adding row/column definitions to the grid
			$InfoWindow_Grid.RowDefinitions.add($NewRow)
			$InfoWindow_Grid.ColumnDefinitions.add($NewColumn_Description)
			$InfoWindow_Grid.ColumnDefinitions.add($NewColumn_Content)
			
			# 3. Creating and adding controls to the grid
			# (by the way: to which grid? "[System.Windows.Controls.Grid]::Set..." doesn't tell)
			
			$label_description         = New-Object System.Windows.Controls.Label
						
			$label_description.Content = $ui[0] + ":"
			$label_description.Name    = ($ui[0]).Replace(' ', '_')
		
			[System.Windows.Controls.Grid]::SetRow($label_description, $i)
			[System.Windows.Controls.Grid]::SetColumn($label_description, 0)
			
			$label_content         = New-Object System.Windows.Controls.Label
			$label_content.Name    = "InfoView_LabelContent$i"
			
			# This case handling has to be done here: When I created $positive_sign/$negative_sign
			# before the loop and assigned that label to $UserInfo array, only one row (the last? )got
			# symbol (of each):
			# Seems you can't assign the same object multiple time (didn't read the fine print...);
			# if you do, a runtime error shows up: "Specified Visual object is already a child of
			# another Visual or the root of a CompositionTarget." (or similar).
			# So, we create new objects here instead, on demand.
			if ($ui[1] -ne $null)
			{
				if ($ui[1].GetType().ToString() -eq "System.Boolean")
				{
					if ($ui[1] -eq $true)
					{
						# Label with ✔ HEAVY CHECK MARK U+2714 (10004)
						$positive_sign = New-Object System.Windows.Controls.Label
						$positive_sign.Content = ([char]10004).ToString()
						$positive_sign.Foreground = "Green"
						$positive_sign.Padding = "0"
						$label_content = $positive_sign
					}
					else
					{
						# Label with ❌ CROSS MARK U+274C
						$negative_sign = New-Object System.Windows.Controls.Label
						$negative_sign.Content = ([char]10060).ToString()
						$negative_sign.Foreground = "Red"
						$negative_sign.Padding = "0"
						$label_content = $negative_sign
					}
				}
				else
				{
					$label_content.Content = $ui[1]
				}
			}
			
			[System.Windows.Controls.Grid]::SetRow($label_content, $i)
			[System.Windows.Controls.Grid]::SetColumn($label_content, 1)
			
			# 4. Finally then adding just the columns as a child to the grid (is the row implictly added?)
			$InfoWindow_Grid.AddChild($label_description)
			$InfoWindow_Grid.AddChild($label_content)
			
			$i++
		}
		
		# --------------------------------------------------------------------------------------
		
		$InfoWindow_Button_OK.add_Click( { $InfoWin.Close() } )

		$InfoWin.ShowDialog()
	}
	else
	{
		[System.Windows.MessageBox]::Show("No match!", "Info", "OK", "Info")
	}
}


# --------------------------------------------------------------------------------------------------
# Function to start the search process (if all requirements [checkboxes...] are met).

function startAction
{
	$domains_to_be_checked = @()

	foreach ($i in $script:checkboxes_domains)
	{
		if ($i.IsChecked)
		{
			$domains_to_be_checked += $i
		}
	}
	
	if ($domains_to_be_checked -ne $null)
	{
		$picked_user = getUserInfo -user $TextBox_Username.Text -domains $domains_to_be_checked.Content
		InfoWindow -user $picked_user
	}
}


# --------------------------------------------------------------------------------------------------
# Further setup work: Control event handling, adjusting properties, etc.

$MainWin.FontFamily = $font.Name
$MainWin.FontSize   = $font.Size

$TextBox_Username.FontFamily = $mono_font.Name
$TextBox_Username.FontSize  += 2

$CheckBox_All.add_Checked(
	{
		ForEach ($c in $checkboxes_domains)
		{
			$c.IsChecked = $true
			$c.IsEnabled = $false
		}
	}
)

$CheckBox_All.add_UnChecked(
	{
		ForEach ($c in $checkboxes_domains)
		{
			$c.IsChecked = $false
			$c.IsEnabled = $true
		}
	}
)

$TextBox_Username.add_TextChanged( { checkState } )

$TextBox_Username.add_KeyDown(
	{
		if ($_.Key -eq 'Return') 
		{
			startAction  
		}
	}
)

$Button_OK.add_Click( { startAction } )

# Display a helpful text in the textbox ("watermark style"), as long as the user hasn't entered anything.
$TextBox_Username.add_GotFocus(
	{
		if ($TextBox_Username.Text -eq $TextBox_Username_InitialText)
		{
			$TextBox_Username.Foreground = 'Black'
			$TextBox_Username.Text = ''
		}
	}
)

# Display a helpful text in the textbox ("watermark style"), as long as the user hasn't entered anything.
$TextBox_Username.add_LostFocus(
	{
		if ($TextBox_Username.Text -eq '')
		{
			$TextBox_Username.Foreground = 'Gray'
			$TextBox_Username.Text = $TextBox_Username_InitialText
		}
		elseif (-not ($TextBox_Username.Text -eq ''))
		{
			$TextBox_Username.Foreground = 'Black'
		}
	}
)

# --------------------------------------------------------------------------------------------------

$null = $MainWin.ShowDialog()